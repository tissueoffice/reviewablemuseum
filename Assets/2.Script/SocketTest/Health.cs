﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{

    public const int maxHealth=100;
    public bool destroyOnDeath;
    public int currentHealth = maxHealth;
    public bool isEnemy=false;

    public RectTransform healthBar;
    public GameObject totlaBar;
    
    private bool isLocalPlayer;

    public GameObject fire;
    private bool IsDied;


    void Start()
    {
        PlayerController p_controller=GetComponent<PlayerController>();
        isLocalPlayer=p_controller.isLocalPlayer;
        fire.SetActive(false);
        IsDied=false;
    }

    public void TakeDamaged(GameObject playerFrom, int amount){
        currentHealth-=amount;
        Debug.Log("currentHealth "+currentHealth);
        NetworkManager networkManager=NetworkManager.instance.GetComponent<NetworkManager>();
        
        if(currentHealth<=0&&!IsDied){
            networkManager.CommandKilled(playerFrom.name, this.gameObject.name);
            Debug.Log("Died"+IsDied);
            IsDied=true;
            otherplayerDied();
        }else{
            networkManager.CommandHealthChange(playerFrom,this.gameObject,amount,isEnemy);   
        }
    } 

    public void otherplayerDied(){
        fire.SetActive(true);
        totlaBar.SetActive(false);
        this.gameObject.GetComponent<PlayerController>().enabled=false;
        this.gameObject.GetComponent<BoxCollider>().enabled=false;
    }

    public void OnChangeHealth(){

        healthBar.sizeDelta=new Vector2(currentHealth,healthBar.sizeDelta.y);
        Debug.Log(healthBar.sizeDelta);

        if(currentHealth<=0){
            if(destroyOnDeath){
                
                Debug.Log("I have to be destroyed");
                

            }else{
               
            currentHealth=maxHealth;
            healthBar.sizeDelta=new Vector2(currentHealth,healthBar.sizeDelta.y);
            Respawn();

            }
        }
    }


    void Respawn(){
        if(isLocalPlayer){
            Vector3 spawnPoint=new Vector3(0,1,0);
            Quaternion spawnRotation=Quaternion.Euler(0,180,0);
            transform.position=spawnPoint;
            transform.rotation=spawnRotation;
            int newhealth=100;
            float [] position ={transform.position.x, transform.position.y, transform.position.z};
            float [] rotation={spawnRotation.x, spawnRotation.y, spawnRotation.z};
            
            NetworkManager networkManager=NetworkManager.instance.GetComponent<NetworkManager>();
            networkManager.CommandRespawn(this.gameObject,position, rotation,newhealth);
        }

    }


 
}
