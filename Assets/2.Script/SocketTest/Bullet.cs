﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    // Start is called before the first frame update
    [HideInInspector]
    public GameObject playerFrom;

    void OnCollisionEnter(Collision collision) {
      
        var hit=collision.gameObject;
        if(hit.transform.tag=="Player"){
        var health=hit.GetComponent<Health>();
          Debug.Log("Collision "+hit.gameObject.name);

            if(health!=null){
            health.TakeDamaged(playerFrom,6);
         }
        }
        

        Destroy(gameObject);    
    }


}
