﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

private static GameManager g_manager;

public static GameManager GetInstance(){
    if(g_manager==null){
        g_manager=new GameManager();
    }
    return g_manager;
}
public bool isStop=false;
public GameObject mainCharacter;
public Mode mode=Mode.START;

public GameObject inputUI;

 
    private void Awake()
    {
        g_manager = this;
        
        
    }

    private void Update() {
        var fps=mainCharacter.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>();
        if(isStop){
            mode=Mode.STOP;
            }else{
            mode=Mode.PLAYING;
            }
        
        if(mode==Mode.STOP){
           fps.LockMovement();
           inputUI.SetActive(true);

        }else{
            fps.UnLockMovement();
            inputUI.SetActive(false);

        }
    }
    
     

}

