using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActionController : MonoBehaviour
{
    
    [SerializeField] private float range;

    private bool pickupActivated=false; 

    private RaycastHit hitinfo;
    private bool isStopMode;
    [SerializeField] private LayerMask layerMask;
    //특정 레이어에만 반응하도록 레어이 마스크 설정

    [SerializeField]
    private Text actionText;
    void Start()
    {
        if(GameManager.GetInstance()!=null){
            isStopMode=GameManager.GetInstance().isStop;
        }
        
    }

    void Update()
    {


        if(Input.GetKeyDown(KeyCode.Tab)){
            GameManager.GetInstance().isStop=!isStopMode;
            isStopMode=GameManager.GetInstance().isStop;
            Debug.Log(isStopMode);
        }

        

        if(!isStopMode&&Physics.Raycast(Camera.main.transform.position,Camera.main.transform.forward,out hitinfo,range,layerMask)){
                
        Debug.Log("I am watching Item"); 
        ItemInfoAppear();

            if(Input.GetKey(KeyCode.E)){
             actionText.text=hitinfo.transform.GetComponent<StoneText>().getText();
            }

        }else{
            
        InfoDisappear();

        }

    
        
    }

   

    private void ItemInfoAppear(){

        pickupActivated=true;
         actionText.gameObject.SetActive(true);
        actionText.text="자세히 보기 (E키)";

    }

    private void InfoDisappear(){ 
        pickupActivated=false; 
        actionText.gameObject.SetActive(false);

    }

    
}
