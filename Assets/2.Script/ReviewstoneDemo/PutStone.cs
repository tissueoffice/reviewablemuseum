﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PutStone : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject characterCamera;
    public Button talkButton;
    public GameObject StoneBox;
    public GameObject SampleStone;
    public int numStone;
    public InputField inputField;
    private List <GameObject> stones=new List<GameObject>();

    public Text UI_Numbutton;

    void Start()
    {
        Button btn = talkButton.GetComponent<Button>();
        UpdateInfo(numStone);
		btn.onClick.AddListener(putStone);
    }

   private void putStone(){
         
         if(inputField.text!=null&&numStone>0){

             GameObject newStone = Instantiate(SampleStone) as GameObject;
            stones.Add(newStone);
            newStone.SetActive(true);
            newStone.transform.SetParent(StoneBox.transform, false);
            newStone.transform.localScale.Set(1, 1, 1);
            newStone.transform.localPosition.Set(0, 0, 0);
            newStone.transform.position=characterCamera.transform.position+characterCamera.transform.forward*3f;
            newStone.GetComponent<StoneText>().setText(inputField.text.ToString());
            inputField.text="";
            inputField.ForceLabelUpdate();
            numStone--;
            UpdateInfo(numStone);
         }
       
   }

   private void UpdateInfo(int numStone){
       UI_Numbutton.text=numStone.ToString()+" /5";
   }
}
