﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class StoneText : MonoBehaviour
{
    public string stoneText;

    StoneText(string text){
        stoneText=text;
    }

    public string getText(){
        return stoneText;
    }

    public void setText(string text){
        stoneText=text;
    }
}
