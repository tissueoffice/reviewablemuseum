﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawn : MonoBehaviour
{
    [SerializeField] private Transform player;
    [SerializeField] private Transform respawnpoint;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
            {
            player.transform.position = respawnpoint.transform.position;
            Physics.SyncTransforms();
        }
    }
}
