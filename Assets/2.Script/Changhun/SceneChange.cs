﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChange : MonoBehaviour
{

    public void ChangeFirstScene()
    {
        SceneManager.LoadScene("01.FirstScene");
    }
    public void ChangeSecondScene()
    {
        SceneManager.LoadScene("02.SecondScene");
    }
    public void ChangeThirdScene()
    {
        SceneManager.LoadScene("03.ThirdScene");
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
