﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameMgr : MonoBehaviour
{
    public InputField inputName;
    public InputField inputNBTItype;
    public InputField inputSleeptime;

    public void Save()
    {
        PlayerPrefs.SetString("Name", inputName.text);
        PlayerPrefs.SetString("NBTItype", inputNBTItype.text);
        PlayerPrefs.SetString("Sleeptime", inputSleeptime.text);
    }

    public void Load()
    {
        if(PlayerPrefs.HasKey("Name"))
        {
            inputName.text = PlayerPrefs.GetString("Name");
            inputNBTItype.text = PlayerPrefs.GetInt("NBTItype").ToString();
            inputSleeptime.text = PlayerPrefs.GetFloat("Sleeptime").ToString();
}
        
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
