<Q                         FOG_EXP2   _ADDITIONAL_LIGHTS     _ADDITIONAL_LIGHT_SHADOWS      _MIXED_LIGHTING_SUBTRACTIVE    _SHADOWS_SOFT       �<  ���$      0                       xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;

#if !(__HAVE_FMA__)
#define fma(a,b,c) ((a) * (b) + (c))
#endif

#ifndef XLT_REMAP_O
	#define XLT_REMAP_O {0, 1, 2, 3, 4, 5, 6, 7}
#endif
constexpr constant uint xlt_remap_o[] = XLT_REMAP_O;
struct _LightBuffer_Type
{
    float4 _MainLightPosition;
    float4 _MainLightColor;
    float4 _AdditionalLightsCount;
    float4 _AdditionalLightsPosition[16];
    float4 _AdditionalLightsColor[16];
    float4 _AdditionalLightsAttenuation[16];
    float4 _AdditionalLightsSpotDir[16];
};

struct UnityPerDraw_Type
{
    float4 hlslcc_mtx4x4unity_ObjectToWorld[4];
    float4 hlslcc_mtx4x4unity_WorldToObject[4];
    float4 unity_LODFade;
    float4 unity_WorldTransformParams;
    float4 unity_LightData;
    float4 unity_LightIndices[2];
    float4 unity_SpecCube0_HDR;
    float4 unity_LightmapST;
    float4 unity_DynamicLightmapST;
    float4 unity_SHAr;
    float4 unity_SHAg;
    float4 unity_SHAb;
    float4 unity_SHBr;
    float4 unity_SHBg;
    float4 unity_SHBb;
    float4 unity_SHC;
};

struct UnityPerFrame_Type
{
    float4 glstate_lightmodel_ambient;
    float4 unity_AmbientSky;
    float4 unity_AmbientEquator;
    float4 unity_AmbientGround;
    float4 unity_IndirectSpecColor;
    float4 unity_FogParams;
    float4 unity_FogColor;
    float4 hlslcc_mtx4x4glstate_matrix_projection[4];
    float4 hlslcc_mtx4x4unity_MatrixV[4];
    float4 hlslcc_mtx4x4unity_MatrixInvV[4];
    float4 hlslcc_mtx4x4unity_MatrixVP[4];
    float4 unity_StereoScaleOffset;
    int unity_StereoEyeIndex;
    float4 unity_ShadowColor;
};

struct _AdditionalLightsShadowBuffer_Type
{
    float4 hlslcc_mtx4x4_AdditionalLightsWorldToShadow[64];
    float _AdditionalShadowStrength[16];
    float4 _AdditionalShadowOffset0;
    float4 _AdditionalShadowOffset1;
    float4 _AdditionalShadowOffset2;
    float4 _AdditionalShadowOffset3;
    float4 _AdditionalShadowmapSize;
};

struct Mtl_FragmentIn
{
    float3 TEXCOORD0 [[ user(TEXCOORD0) ]] ;
    float4 TEXCOORD1 [[ user(TEXCOORD1) ]] ;
    float3 TEXCOORD3 [[ user(TEXCOORD3) ]] ;
    float3 TEXCOORD4 [[ user(TEXCOORD4) ]] ;
    float3 TEXCOORD7 [[ user(TEXCOORD7) ]] ;
    float4 COLOR0 [[ user(COLOR0) ]] ;
    float4 TEXCOORD8 [[ user(TEXCOORD8) ]] ;
};

struct Mtl_FragmentOut
{
    float4 SV_Target0 [[ color(xlt_remap_o[0]) ]];
};

constexpr sampler _mtl_xl_shadow_sampler(address::clamp_to_edge, filter::linear, compare_func::greater_equal);
fragment Mtl_FragmentOut xlatMtlMain(
    constant _LightBuffer_Type& _LightBuffer [[ buffer(0) ]],
    constant UnityPerDraw_Type& UnityPerDraw [[ buffer(1) ]],
    constant UnityPerFrame_Type& UnityPerFrame [[ buffer(2) ]],
    constant _AdditionalLightsShadowBuffer_Type& _AdditionalLightsShadowBuffer [[ buffer(3) ]],
    sampler samplerunity_SpecCube0 [[ sampler (0) ]],
    sampler sampler_AdditionalLightsShadowmapTexture [[ sampler (1) ]],
    sampler samplerTexture2D_BF5F6835 [[ sampler (2) ]],
    texturecube<float, access::sample > unity_SpecCube0 [[ texture(0) ]] ,
    depth2d<float, access::sample > _AdditionalLightsShadowmapTexture [[ texture(1) ]] ,
    texture2d<float, access::sample > Texture2D_BF5F6835 [[ texture(2) ]] ,
    Mtl_FragmentIn input [[ stage_in ]])
{
    Mtl_FragmentOut output;
    float3 u_xlat0;
    float3 u_xlat1;
    float3 u_xlat2;
    float4 u_xlat3;
    float3 u_xlat4;
    float3 u_xlat5;
    float3 u_xlat6;
    float4 u_xlat7;
    float2 u_xlat8;
    float4 u_xlat9;
    float4 u_xlat10;
    float4 u_xlat11;
    float4 u_xlat12;
    float4 u_xlat13;
    float4 u_xlat14;
    float4 u_xlat15;
    float3 u_xlat16;
    float2 u_xlat40;
    float2 u_xlat42;
    float u_xlat48;
    int u_xlati48;
    float u_xlat49;
    int u_xlati49;
    float u_xlat50;
    int u_xlati50;
    bool u_xlatb50;
    float u_xlat51;
    bool u_xlatb51;
    float u_xlat52;
    float u_xlat53;
    int u_xlati53;
    float u_xlat54;
    bool u_xlatb54;
    u_xlat0.xyz = Texture2D_BF5F6835.sample(samplerTexture2D_BF5F6835, input.TEXCOORD8.xy).xyz;
    u_xlat0.xyz = u_xlat0.xyz * input.COLOR0.xyz;
    u_xlat48 = dot(input.TEXCOORD4.xyz, input.TEXCOORD4.xyz);
    u_xlat48 = rsqrt(u_xlat48);
    u_xlat1.xyz = float3(u_xlat48) * input.TEXCOORD4.xyz;
    u_xlat48 = dot(input.TEXCOORD7.xyz, input.TEXCOORD7.xyz);
    u_xlat48 = rsqrt(u_xlat48);
    u_xlat2.xyz = float3(u_xlat48) * input.TEXCOORD7.xyz;
    u_xlat0.xyz = u_xlat0.xyz * float3(0.959999979, 0.959999979, 0.959999979);
    u_xlat49 = dot((-u_xlat2.xyz), u_xlat1.xyz);
    u_xlat49 = u_xlat49 + u_xlat49;
    u_xlat3.xyz = fma(u_xlat1.xyz, (-float3(u_xlat49)), (-u_xlat2.xyz));
    u_xlat49 = dot(u_xlat1.xyz, u_xlat2.xyz);
    u_xlat49 = clamp(u_xlat49, 0.0f, 1.0f);
    u_xlat49 = (-u_xlat49) + 1.0;
    u_xlat49 = u_xlat49 * u_xlat49;
    u_xlat49 = u_xlat49 * u_xlat49;
    u_xlat3 = unity_SpecCube0.sample(samplerunity_SpecCube0, u_xlat3.xyz, level(4.05000019));
    u_xlat50 = u_xlat3.w + -1.0;
    u_xlat50 = fma(UnityPerDraw.unity_SpecCube0_HDR.w, u_xlat50, 1.0);
    u_xlat50 = max(u_xlat50, 0.0);
    u_xlat50 = log2(u_xlat50);
    u_xlat50 = u_xlat50 * UnityPerDraw.unity_SpecCube0_HDR.y;
    u_xlat50 = exp2(u_xlat50);
    u_xlat50 = u_xlat50 * UnityPerDraw.unity_SpecCube0_HDR.x;
    u_xlat3.xyz = u_xlat3.xyz * float3(u_xlat50);
    u_xlat3.xyz = u_xlat3.xyz * float3(0.941176474, 0.941176474, 0.941176474);
    u_xlat49 = fma(u_xlat49, 0.5, 0.0399999991);
    u_xlat3.xyz = float3(u_xlat49) * u_xlat3.xyz;
    u_xlat3.xyz = fma(input.TEXCOORD0.xyz, u_xlat0.xyz, u_xlat3.xyz);
    u_xlat49 = dot(u_xlat1.xyz, _LightBuffer._MainLightPosition.xyz);
    u_xlat49 = clamp(u_xlat49, 0.0f, 1.0f);
    u_xlat49 = u_xlat49 * UnityPerDraw.unity_LightData.z;
    u_xlat4.xyz = float3(u_xlat49) * _LightBuffer._MainLightColor.xyz;
    u_xlat5.xyz = fma(input.TEXCOORD7.xyz, float3(u_xlat48), _LightBuffer._MainLightPosition.xyz);
    u_xlat48 = dot(u_xlat5.xyz, u_xlat5.xyz);
    u_xlat48 = max(u_xlat48, 1.17549435e-38);
    u_xlat48 = rsqrt(u_xlat48);
    u_xlat5.xyz = float3(u_xlat48) * u_xlat5.xyz;
    u_xlat48 = dot(u_xlat1.xyz, u_xlat5.xyz);
    u_xlat48 = clamp(u_xlat48, 0.0f, 1.0f);
    u_xlat49 = dot(_LightBuffer._MainLightPosition.xyz, u_xlat5.xyz);
    u_xlat49 = clamp(u_xlat49, 0.0f, 1.0f);
    u_xlat48 = u_xlat48 * u_xlat48;
    u_xlat48 = fma(u_xlat48, -0.9375, 1.00001001);
    u_xlat49 = u_xlat49 * u_xlat49;
    u_xlat48 = u_xlat48 * u_xlat48;
    u_xlat49 = max(u_xlat49, 0.100000001);
    u_xlat48 = u_xlat48 * u_xlat49;
    u_xlat48 = u_xlat48 * 3.0;
    u_xlat48 = 0.0625 / u_xlat48;
    u_xlat5.xyz = fma(float3(u_xlat48), float3(0.0399999991, 0.0399999991, 0.0399999991), u_xlat0.xyz);
    u_xlat3.xyz = fma(u_xlat5.xyz, u_xlat4.xyz, u_xlat3.xyz);
    u_xlat48 = min(_LightBuffer._AdditionalLightsCount.x, UnityPerDraw.unity_LightData.y);
    u_xlati48 = int(u_xlat48);
    u_xlat4.xyz = u_xlat3.xyz;
    u_xlati49 = 0x0;
    while(true){
        u_xlatb50 = u_xlati49>=u_xlati48;
        if(u_xlatb50){break;}
        u_xlat50 = float(u_xlati49);
        u_xlatb51 = u_xlat50<2.0;
        u_xlat5.xy = (bool(u_xlatb51)) ? UnityPerDraw.unity_LightIndices[0].xy : UnityPerDraw.unity_LightIndices[0].zw;
        u_xlat52 = u_xlat50 + -2.0;
        u_xlat50 = (u_xlatb51) ? u_xlat50 : u_xlat52;
        u_xlatb50 = u_xlat50<1.0;
        u_xlat50 = (u_xlatb50) ? u_xlat5.x : u_xlat5.y;
        u_xlati50 = int(u_xlat50);
        u_xlat5.xyz = (-input.TEXCOORD3.xyz) + _LightBuffer._AdditionalLightsPosition[u_xlati50].xyz;
        u_xlat51 = dot(u_xlat5.xyz, u_xlat5.xyz);
        u_xlat51 = max(u_xlat51, 6.10351562e-05);
        u_xlat52 = rsqrt(u_xlat51);
        u_xlat6.xyz = float3(u_xlat52) * u_xlat5.xyz;
        u_xlat53 = float(1.0) / float(u_xlat51);
        u_xlat51 = u_xlat51 * _LightBuffer._AdditionalLightsAttenuation[u_xlati50].x;
        u_xlat51 = fma((-u_xlat51), u_xlat51, 1.0);
        u_xlat51 = max(u_xlat51, 0.0);
        u_xlat51 = u_xlat51 * u_xlat51;
        u_xlat51 = u_xlat51 * u_xlat53;
        u_xlat53 = dot(_LightBuffer._AdditionalLightsSpotDir[u_xlati50].xyz, u_xlat6.xyz);
        u_xlat53 = fma(u_xlat53, _LightBuffer._AdditionalLightsAttenuation[u_xlati50].z, _LightBuffer._AdditionalLightsAttenuation[u_xlati50].w);
        u_xlat53 = clamp(u_xlat53, 0.0f, 1.0f);
        u_xlat53 = u_xlat53 * u_xlat53;
        u_xlat51 = u_xlat51 * u_xlat53;
        u_xlati53 = u_xlati50 << 0x2;
        u_xlat7 = input.TEXCOORD3.yyyy * _AdditionalLightsShadowBuffer.hlslcc_mtx4x4_AdditionalLightsWorldToShadow[(u_xlati53 + 1)];
        u_xlat7 = fma(_AdditionalLightsShadowBuffer.hlslcc_mtx4x4_AdditionalLightsWorldToShadow[u_xlati53], input.TEXCOORD3.xxxx, u_xlat7);
        u_xlat7 = fma(_AdditionalLightsShadowBuffer.hlslcc_mtx4x4_AdditionalLightsWorldToShadow[(u_xlati53 + 2)], input.TEXCOORD3.zzzz, u_xlat7);
        u_xlat7 = u_xlat7 + _AdditionalLightsShadowBuffer.hlslcc_mtx4x4_AdditionalLightsWorldToShadow[(u_xlati53 + 3)];
        u_xlat7.xyz = u_xlat7.xyz / u_xlat7.www;
        u_xlat8.xy = fma(u_xlat7.xy, _AdditionalLightsShadowBuffer._AdditionalShadowmapSize.zw, float2(0.5, 0.5));
        u_xlat8.xy = floor(u_xlat8.xy);
        u_xlat7.xy = fma(u_xlat7.xy, _AdditionalLightsShadowBuffer._AdditionalShadowmapSize.zw, (-u_xlat8.xy));
        u_xlat9 = u_xlat7.xxyy + float4(0.5, 1.0, 0.5, 1.0);
        u_xlat10 = u_xlat9.xxzz * u_xlat9.xxzz;
        u_xlat40.xy = u_xlat10.yw * float2(0.0799999982, 0.0799999982);
        u_xlat9.xz = fma(u_xlat10.xz, float2(0.5, 0.5), (-u_xlat7.xy));
        u_xlat10.xy = (-u_xlat7.xy) + float2(1.0, 1.0);
        u_xlat42.xy = min(u_xlat7.xy, float2(0.0, 0.0));
        u_xlat42.xy = fma((-u_xlat42.xy), u_xlat42.xy, u_xlat10.xy);
        u_xlat7.xy = max(u_xlat7.xy, float2(0.0, 0.0));
        u_xlat7.xy = fma((-u_xlat7.xy), u_xlat7.xy, u_xlat9.yw);
        u_xlat42.xy = u_xlat42.xy + float2(1.0, 1.0);
        u_xlat7.xy = u_xlat7.xy + float2(1.0, 1.0);
        u_xlat11.xy = u_xlat9.xz * float2(0.159999996, 0.159999996);
        u_xlat12.xy = u_xlat10.xy * float2(0.159999996, 0.159999996);
        u_xlat10.xy = u_xlat42.xy * float2(0.159999996, 0.159999996);
        u_xlat13.xy = u_xlat7.xy * float2(0.159999996, 0.159999996);
        u_xlat7.xy = u_xlat9.yw * float2(0.159999996, 0.159999996);
        u_xlat11.z = u_xlat10.x;
        u_xlat11.w = u_xlat7.x;
        u_xlat12.z = u_xlat13.x;
        u_xlat12.w = u_xlat40.x;
        u_xlat9 = u_xlat11.zwxz + u_xlat12.zwxz;
        u_xlat10.z = u_xlat11.y;
        u_xlat10.w = u_xlat7.y;
        u_xlat13.z = u_xlat12.y;
        u_xlat13.w = u_xlat40.y;
        u_xlat7.xyw = u_xlat10.zyw + u_xlat13.zyw;
        u_xlat10.xyz = u_xlat12.xzw / u_xlat9.zwy;
        u_xlat10.xyz = u_xlat10.xyz + float3(-2.5, -0.5, 1.5);
        u_xlat11.xyz = u_xlat13.zyw / u_xlat7.xyw;
        u_xlat11.xyz = u_xlat11.xyz + float3(-2.5, -0.5, 1.5);
        u_xlat10.xyz = u_xlat10.yxz * _AdditionalLightsShadowBuffer._AdditionalShadowmapSize.xxx;
        u_xlat11.xyz = u_xlat11.xyz * _AdditionalLightsShadowBuffer._AdditionalShadowmapSize.yyy;
        u_xlat10.w = u_xlat11.x;
        u_xlat12 = fma(u_xlat8.xyxy, _AdditionalLightsShadowBuffer._AdditionalShadowmapSize.xyxy, u_xlat10.ywxw);
        u_xlat40.xy = fma(u_xlat8.xy, _AdditionalLightsShadowBuffer._AdditionalShadowmapSize.xy, u_xlat10.zw);
        u_xlat11.w = u_xlat10.y;
        u_xlat10.yw = u_xlat11.yz;
        u_xlat13 = fma(u_xlat8.xyxy, _AdditionalLightsShadowBuffer._AdditionalShadowmapSize.xyxy, u_xlat10.xyzy);
        u_xlat11 = fma(u_xlat8.xyxy, _AdditionalLightsShadowBuffer._AdditionalShadowmapSize.xyxy, u_xlat11.wywz);
        u_xlat10 = fma(u_xlat8.xyxy, _AdditionalLightsShadowBuffer._AdditionalShadowmapSize.xyxy, u_xlat10.xwzw);
        u_xlat14 = u_xlat7.xxxy * u_xlat9.zwyz;
        u_xlat15 = u_xlat7.yyww * u_xlat9;
        u_xlat53 = u_xlat7.w * u_xlat9.y;
        u_xlat54 = _AdditionalLightsShadowmapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat12.xy, saturate(u_xlat7.z), level(0.0));
        u_xlat7.x = _AdditionalLightsShadowmapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat12.zw, saturate(u_xlat7.z), level(0.0));
        u_xlat7.x = u_xlat7.x * u_xlat14.y;
        u_xlat54 = fma(u_xlat14.x, u_xlat54, u_xlat7.x);
        u_xlat7.x = _AdditionalLightsShadowmapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat40.xy, saturate(u_xlat7.z), level(0.0));
        u_xlat54 = fma(u_xlat14.z, u_xlat7.x, u_xlat54);
        u_xlat7.x = _AdditionalLightsShadowmapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat11.xy, saturate(u_xlat7.z), level(0.0));
        u_xlat54 = fma(u_xlat14.w, u_xlat7.x, u_xlat54);
        u_xlat7.x = _AdditionalLightsShadowmapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat13.xy, saturate(u_xlat7.z), level(0.0));
        u_xlat54 = fma(u_xlat15.x, u_xlat7.x, u_xlat54);
        u_xlat7.x = _AdditionalLightsShadowmapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat13.zw, saturate(u_xlat7.z), level(0.0));
        u_xlat54 = fma(u_xlat15.y, u_xlat7.x, u_xlat54);
        u_xlat7.x = _AdditionalLightsShadowmapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat11.zw, saturate(u_xlat7.z), level(0.0));
        u_xlat54 = fma(u_xlat15.z, u_xlat7.x, u_xlat54);
        u_xlat7.x = _AdditionalLightsShadowmapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat10.xy, saturate(u_xlat7.z), level(0.0));
        u_xlat54 = fma(u_xlat15.w, u_xlat7.x, u_xlat54);
        u_xlat7.x = _AdditionalLightsShadowmapTexture.sample_compare(_mtl_xl_shadow_sampler, u_xlat10.zw, saturate(u_xlat7.z), level(0.0));
        u_xlat53 = fma(u_xlat53, u_xlat7.x, u_xlat54);
        u_xlat54 = 1.0 + (-_AdditionalLightsShadowBuffer._AdditionalShadowStrength[u_xlati50]);
        u_xlat53 = fma(u_xlat53, _AdditionalLightsShadowBuffer._AdditionalShadowStrength[u_xlati50], u_xlat54);
        u_xlatb54 = 0.0>=u_xlat7.z;
        u_xlat53 = (u_xlatb54) ? 1.0 : u_xlat53;
        u_xlat51 = u_xlat51 * u_xlat53;
        u_xlat53 = dot(u_xlat1.xyz, u_xlat6.xyz);
        u_xlat53 = clamp(u_xlat53, 0.0f, 1.0f);
        u_xlat51 = u_xlat51 * u_xlat53;
        u_xlat7.xyz = float3(u_xlat51) * _LightBuffer._AdditionalLightsColor[u_xlati50].xyz;
        u_xlat5.xyz = fma(u_xlat5.xyz, float3(u_xlat52), u_xlat2.xyz);
        u_xlat50 = dot(u_xlat5.xyz, u_xlat5.xyz);
        u_xlat50 = max(u_xlat50, 1.17549435e-38);
        u_xlat50 = rsqrt(u_xlat50);
        u_xlat5.xyz = float3(u_xlat50) * u_xlat5.xyz;
        u_xlat50 = dot(u_xlat1.xyz, u_xlat5.xyz);
        u_xlat50 = clamp(u_xlat50, 0.0f, 1.0f);
        u_xlat51 = dot(u_xlat6.xyz, u_xlat5.xyz);
        u_xlat51 = clamp(u_xlat51, 0.0f, 1.0f);
        u_xlat50 = u_xlat50 * u_xlat50;
        u_xlat50 = fma(u_xlat50, -0.9375, 1.00001001);
        u_xlat51 = u_xlat51 * u_xlat51;
        u_xlat50 = u_xlat50 * u_xlat50;
        u_xlat51 = max(u_xlat51, 0.100000001);
        u_xlat50 = u_xlat50 * u_xlat51;
        u_xlat50 = u_xlat50 * 3.0;
        u_xlat50 = 0.0625 / u_xlat50;
        u_xlat5.xyz = fma(float3(u_xlat50), float3(0.0399999991, 0.0399999991, 0.0399999991), u_xlat0.xyz);
        u_xlat4.xyz = fma(u_xlat5.xyz, u_xlat7.xyz, u_xlat4.xyz);
        u_xlati49 = u_xlati49 + 0x1;
    }
    u_xlat0.x = input.TEXCOORD1.x * (-input.TEXCOORD1.x);
    u_xlat0.x = exp2(u_xlat0.x);
    u_xlat16.xyz = u_xlat4.xyz + (-UnityPerFrame.unity_FogColor.xyz);
    output.SV_Target0.xyz = fma(u_xlat0.xxx, u_xlat16.xyz, UnityPerFrame.unity_FogColor.xyz);
    output.SV_Target0.w = 1.0;
    return output;
}
                              _LightBuffer0        _MainLightPosition                           _MainLightColor                         _AdditionalLightsCount                           _AdditionalLightsPosition                    0      _AdditionalLightsColor                   0     _AdditionalLightsAttenuation                 0     _AdditionalLightsSpotDir                 0         UnityPerDrawp        unity_LightData                   �      unity_LightIndices                   �      unity_SpecCube0_HDR                   �          UnityPerFrame   �        unity_FogColor                    `          _AdditionalLightsShadowBuffer   �        _AdditionalShadowStrength                          _AdditionalShadowmapSize                  �     _AdditionalLightsWorldToShadow                                unity_SpecCube0                !   _AdditionalLightsShadowmapTexture                   Texture2D_BF5F6835                  _LightBuffer              UnityPerDraw             UnityPerFrame                _AdditionalLightsShadowBuffer             