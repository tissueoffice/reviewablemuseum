var app=require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);

server.listen(8080);

//global variable for the server

var enemies = [];
var playerSpawnPoints=[];
var clients=[];
var killerchart=[];

app.get('/',function(req,res){
    res.send('hey you got bak get"/" ')
});

io.on('connection',function(socket){

    var currentPlayer={};
    currentPlayer.name='unknown';
    
    socket.on('player connect',function(){
        console.log(currentPlayer.name+' recv :player connect');
        for(var i =0;i<clients.length;i++){
            var playerConnected={
            name: clients[i].name,
            position:clients[i].position,
            rotation:clients[i].rotation,
            health: clients[i].health 
            };
            socket.emit('other player connected', playerConnected);
            //내가 들어오면 이미 있던 플레이어들의 정보가 나에게 온다 
            console.log(currentPlayer.name+' emit: other player connected'+JSON.stringify(playerConnected));
        }
        
    });
    socket.on('play',function(data){
        console.log(currentPlayer.name+' recv : player '+JSON.stringify(data));
    
        if(clients.length===0){
            //내가 혼자 들어오면 가상 적들이 있다. 
            numberOfEnemies=data.enemySpawnPoints.length;
            enemies=[];
            data.enemySpawnPoints.forEach(function(enemySpawnPoint){
                var enemy ={
                        name : guid(),
                        position : enemySpawnPoint.position,
                        rotation : enemySpawnPoint.rotation,
                        health:100
                };
                enemies.push(enemy);
            });
            playerSpawnPoints=[];
            data.playerSpawnPoints.forEach(function(_playerSpawnPoint){
                var playerSpawnPoint ={
                    //playerSpawnpoint를 받아서 서버에 넣는다 
                    position : _playerSpawnPoint.position,
                    rotation : _playerSpawnPoint.rotation
                };
                playerSpawnPoints.push(playerSpawnPoint);
            });

        }
        var enemiesResponse={
            //적들의 움직임? 
            enemies:enemies
        };
        console.log(currentPlayer.name+' emit:enemies: '+JSON.stringify(enemiesResponse));
        socket.emit('enemies',enemiesResponse);

        var randomSpawnPoint=playerSpawnPoints[Math.floor(Math.random()*playerSpawnPoints.length)];
       //내가 들어갈때 나한테 랜덤한 spawn point가 주어진다 
        currentPlayer={
            name: data.name,
            position: randomSpawnPoint.position,
            rotation : randomSpawnPoint.rotation,
            health : 100
        };
        clients.push(currentPlayer);
        console.log(currentPlayer.name+' emit:play: '+JSON.stringify(currentPlayer));
        socket.emit('play',currentPlayer);
        // in your current game, we need to tell the other players about you 
        socket.broadcast.emit('other player connected', currentPlayer);
    });
    socket.on('player move',function(data){
        //console.log('recv : move: '+JSON.stringify(data));
        currentPlayer.position=data.position;
        socket.broadcast.emit('player move',currentPlayer);
    });
    socket.on('player turn',function(data){
        //console.log('recv : turn: '+JSON.stringify(data));
        currentPlayer.rotation=data.rotation;
        socket.broadcast.emit('player turn',currentPlayer);
        //플레이어들이 움직이거나, 회전할때마다 정보를 모두에게 보내준다
    });
    socket.on('player shoot',function(){
        //console.log(currentPlayer.name+' recv : shoot');
        var data={
            name: currentPlayer.name
        };
         console.log(currentPlayer.name+': bcst: shoot: '+JSON.stringify(data));
         socket.emit('player shoot',data);
         socket.broadcast.emit('player shoot',data);
    });
    socket.on('player talk',function(data){
        
         console.log(currentPlayer.name+': bcst: talk: '+JSON.stringify(data));
         socket.emit('player talk',data);
         socket.broadcast.emit('player talk',data);
    });
    socket.on('health change',function(data){
        console.log(currentPlayer.name+' recv:health: '+JSON.stringify(data));
        if(data.from ===currentPlayer.name){
            var indexDamaged=0;
            if(!data.isEnemy){
                clients=clients.map(function(clients,index){
                    if(clients.name===data.name){
                        indexDamaged=index;
                        clients.health-=data.healthChange;
                    }
                    return clients;
                });
            }else{
                enemies=enemies.map(function(enemy,index){
                    if(enemy.name===data.name){
                        indexDamaged=index;
                        enemy.health-=data.healthChange;
                    }
                    return enemy;
                });
            }

            var response={
                name : (!data.isEnemy) ? clients[indexDamaged].name : enemies[indexDamaged].name,
                health: (!data.isEnemy) ? clients[indexDamaged].health: enemies[indexDamaged].health
            };
            console.log(currentPlayer.name+' bcst : health : '+JSON.stringify(response));
            socket.emit('health change',response);
            socket.broadcast.emit('health change',response);
        }
    });

    socket.on('kill',function(data){
        console.log("killing info"+JSON.stringify(data));
       
        socket.broadcast.emit('kill',data);
    });
    socket.on('respawn',function(data){
        if(data.name === currentPlayer.name){
            currentPlayer.position=data.position;
            currentPlayer.rotation=data.rotation;
            currentPlayer.health=data.health;
            var response={
                name : currentPlayer.name,
                health: currentPlayer.health
            };
        }else{return;}
        
        socket.emit('health change',response);
        socket.broadcast.emit('health change',response);
        
    });
    socket.on('disconnect',function(){
        console.log(currentPlayer.name+' recv: disconnect '+currentPlayer.name);
        socket.emit('other player disconnected',currentPlayer);
        socket.broadcast.emit('other player disconnected',currentPlayer);
        console.log(currentPlayer.name+ ' bcst: other player disconnected'+JSON.stringify(currentPlayer));
        for(var i=0;i<clients.length;i++){
            if(clients[i].name===currentPlayer.name){
                clients.splice(i,1);
            }
        }
    });
});

console.log('---server is running...'+ guid());
function guid(){
    //랜덤 코드 생성
    function s4(){
        return Math.floor((1+Math.random())*0x10000).toString(16).substring(1);
    }
    return s4()+s4()+'-'+s4()+'-'+s4()+'-'+s4()+s4()+s4();
}